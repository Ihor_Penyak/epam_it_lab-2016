using System;
using System.Text;

namespace Task02
{
    class Rectangle
    {
        private Point _topLeft;
        private Point _bottomRight;        
        
        // IP: ������������ ������-������� � ���� ���� ���, ���� � ������� "get" ��/��� "set" ������������� ����� ����������� �����
		// � ������ ������� ������� �� ��, �� ����� ���� �������� ������� ���������� auto properties  a-la
		// public Point TopLeft {get; set; }
		// ��� ����, �� � ���� �� ����������� ������ ������������, �� �������� � �������� Point
		public Point TopLeft
        {
            get 
            {
                return this._topLeft;
            }            
            set
            {
                this._topLeft = value;
            }            
        }        
        
        public Point BottomRight
        {
            get 
            {
                return this._bottomRight;
            }            
            set
            {
                this._bottomRight = value;
            }            
        }    
        
        public Point BottomLeft
        {
            get
            {
                return new Point { X = this._topLeft.X, Y = this._bottomRight.Y    };
            }            
        }
        
        public Point TopRight 
        {
            get
            {
                return new Point { X = this._bottomRight.X, Y = this._topLeft.Y    };
            }            
        }
        
        public int Width 
        {
            get
            {
                return this.BottomRight.X - this.TopLeft.X;
            }            
        }
        
        public int Height 
        {
            get
            {
                return this.TopLeft.Y - this.BottomRight.Y;
            }            
        }

        public void MoveLeft(int step)
        {
            this.TopLeft.X     -= step;
            this.BottomRight.X -= step;
        }
            
        public void MoveRight(int step)
        {
            this.TopLeft.X     += step;
            this.BottomRight.X += step;
        }    
        
        public void MoveUp(int step)
        {
            this.TopLeft.Y     += step;
            this.BottomRight.Y += step;
        }    
        
        public void MoveDown(int step)
        {
            this.TopLeft.Y     -= step;
            this.BottomRight.Y -= step;
        }
        
        public static Rectangle IntersectionOfRectangles(Rectangle fRect, Rectangle sRect)
        {
            // IP: ��� ������ �������� �� ��������� ����� �-�� "return" � ����� ������ ������ �-���
			// � ������ �� �� ����. ��� ����� ���� � ���������� ���:
			/*
			return (Rectangle.IsIntersection(fRect, sRect)) ? new Rectangle
									                            {
									                                TopLeft = new Point
									                                {
									                                    X = Math.Max(fRect.TopLeft.X, sRect.TopLeft.X),
									                                    Y = Math.Min(fRect.TopLeft.Y, sRect.TopLeft.Y)
									                                },
									                                BottomRight = new Point
									                                {
									                                    X = Math.Min(fRect.BottomRight.X, sRect.BottomRight.X),
									                                    Y = Math.Max(fRect.BottomRight.Y, sRect.BottomRight.Y)
									                                }
									                            } : null;
			*/
			
			if( Rectangle.IsIntersection(fRect, sRect) )        
            {
                return null;
            }
            else 
            {                
                return new Rectangle
                            {
                                TopLeft     = new Point { X = Math.Max(fRect.TopLeft.X, sRect.TopLeft.X ), 
                                                          Y = Math.Min(fRect.TopLeft.Y, sRect.TopLeft.Y )  
                                                        }, 
                                BottomRight = new Point { X = Math.Min(fRect.BottomRight.X, sRect.BottomRight.X ),  
                                                          Y = Math.Max(fRect.BottomRight.Y, sRect.BottomRight.Y ) 
                                                        }
                            }; 
            }
        }
        
        
        public static Rectangle UnionOfRectangles(Rectangle fRect, Rectangle sRect)
        {
            int topLeftX     = Math.Min(fRect.TopLeft.X, sRect.TopLeft.X);
            int topLeftY     = Math.Max(fRect.TopLeft.Y, sRect.TopLeft.Y);
            int bottomRightX = Math.Max(fRect.BottomRight.X, sRect.BottomRight.X);
            int bottomRightY = Math.Min(fRect.BottomRight.Y, sRect.BottomRight.Y);
            
            Rectangle unionRectangle = new Rectangle
                            {
                                TopLeft     = new Point { X = topLeftX,     Y = topLeftY },                                                         
                                BottomRight = new Point { X = bottomRightX, Y = bottomRightY }
                            };
            
            return unionRectangle;
        }
        
        public void IncreaseWidth(int step)
        {
            this.BottomRight.X += step;
        }
        
        public void DecreaseWidth(int step)
        {
            if( this.Width > step ) 
            {
                this.BottomRight.X -= step;
            }
            else {
                Console.WriteLine("Can not decrease width. Current width is less");
            }
        }
        
        public void IncreaseHeight(int step)
        {
            this.TopLeft.Y += step;
        }
        
        public void DecreaseHeight(int step)
        {
            if( this.Height > step ) 
            {
                this.TopLeft.Y -= step;
            }
            else {
                Console.WriteLine("Can not decrease height. Current height is less");
            }
        }
        
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
                        
            result.Append( String.Format("{0,3}----------{1,3}\n", this.TopLeft, this.TopRight ));                     
            result.Append( String.Format("   |              | \n" ));            
            result.Append( String.Format("   |              | \n" ));
            result.Append( String.Format("   |              | \n" ));
            result.Append( String.Format("{0,3}----------{1,3}\n", this.BottomLeft, this.BottomRight ));
                        
            return result.ToString();
        }
        
        private static bool IsIntersection(Rectangle fRect, Rectangle sRect)
        {
            return (fRect.BottomRight.Y > sRect.TopLeft.Y     ||
                    fRect.TopLeft.Y     < sRect.BottomRight.Y ||
                    fRect.TopLeft.X     > sRect.BottomRight.X || 
                    fRect.BottomRight.X < sRect.TopLeft.X            
                );
        }
        
    }
    
    
}