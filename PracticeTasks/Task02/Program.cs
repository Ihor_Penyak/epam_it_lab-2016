using System;

namespace Task02
{
    class Program
    {
        
        static void Main()
        {
            Console.WriteLine("--------------------");
            
            //1. Rectangle Initialization
            
            // IP: � ���� � �� ����������� ��� ����� ������ � ���� ����������� ?  =)
			Rectangle firstRect = new Rectangle
            {
                TopLeft     = new Point { X = -2, Y = 5  }, 
                BottomRight = new Point { X = 3,  Y = 1 }
            };
            
            Rectangle secondRect = new Rectangle
            {
                TopLeft     = new Point { X = 2, Y = 7 }, 
                BottomRight = new Point { X = 7, Y = 3 }
            };
            
            Rectangle thirdRect = new Rectangle
            {
                TopLeft     = new Point { X = 4, Y = 10}, 
                BottomRight = new Point { X = 5, Y = 1 }
            };
            
			// IP: ���������� ��������, �� � ����� �-�� ��������� ������ �1  =) - ������������ ������� ���������
                    
            
            //2. Moving Rectangle
            Console.WriteLine("2. Moving Rectangle");
            Console.WriteLine("Move firstRect to Up at 'step' coordinates: firstRect.MoveUp(3):");
            firstRect.MoveUp(3);
            Console.WriteLine(firstRect);
            Console.WriteLine();
                        
            Console.WriteLine("Move firstRect to Left at 'step' coordinates: firstRect.MoveLeft(1):");
            firstRect.MoveLeft(1);
            Console.WriteLine(firstRect);
            Console.WriteLine();
            
            Console.WriteLine("Move firstRect to Down at 'step' coordinates: firstRect.MoveDown(2):");
            firstRect.MoveDown(2);
            Console.WriteLine(firstRect);
            
            Console.WriteLine("Move firstRect to Right at 'step' coordinates: firstRect.MoveRight(10):");
            firstRect.MoveRight(50);
            Console.WriteLine(firstRect);
            
            //3. Changing dimentions
            Console.WriteLine("3. Changing dimentions");
            Console.WriteLine("Increase/Decrease width or height firstRect");
            Console.WriteLine(firstRect);
            
            Console.WriteLine("IncreaseWidth by 'step': firstRect.IncreaseWidth(3) ");            
            firstRect.IncreaseWidth(3);
            Console.WriteLine(firstRect);
            Console.WriteLine("Current Width = {0}", firstRect.Width);
            Console.WriteLine();
            
            Console.WriteLine("DecreaseWidth by 'step': firstRect.DecreaseWidth(5) ");            
            firstRect.DecreaseWidth(10);
            Console.WriteLine(firstRect);
            Console.WriteLine("Current Width = {0}", firstRect.Width);
            Console.WriteLine();
                        
            Console.WriteLine("IncreaseHeight by 'step': firstRect.IncreaseHeight(3) ");            
            firstRect.IncreaseHeight(3);
            Console.WriteLine(firstRect);
            Console.WriteLine("Current Height = {0}", firstRect.Height);
            Console.WriteLine();
            
            Console.WriteLine("DecreaseHeight by 'step': firstRect.DecreaseHeight(5) ");            
            firstRect.DecreaseHeight(5);
            Console.WriteLine(firstRect);
            Console.WriteLine("Current Height = {0}", firstRect.Height);
            Console.WriteLine();
                                    
            
            //4. Finding intersection of two rectangles
            Console.WriteLine("4. Finding intersection of two rectangles");
                        
            Rectangle intersection = Rectangle.IntersectionOfRectangles(firstRect, secondRect);
            
            Console.WriteLine("Result for firstRect and secondRect: ");            
            Console.WriteLine( intersection == null ? "Rectangles are not intersecting" : intersection.ToString() );
            Console.WriteLine();
            
            intersection = Rectangle.IntersectionOfRectangles(secondRect, thirdRect);
            
            Console.WriteLine("Result for secondRect and thirdRect: ");
            Console.WriteLine( intersection == null ? "Rectangles are not intersecting" : intersection.ToString() );
            Console.WriteLine();
            
            //5. Finding union of two rectangles
            Console.WriteLine("5. Finding union of two rectangles");
            Console.WriteLine("Result for secondRect and thirdRect: ");
            Console.WriteLine( Rectangle.UnionOfRectangles(secondRect, thirdRect) );
            Console.WriteLine();
            
            
            Console.ReadLine();
            
            return;
        }
        
        
    }
    
}