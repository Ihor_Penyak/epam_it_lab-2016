using System;
using System.Text;

namespace Task03
{
    class ArrayVector
    {    
        private uint _startIndex;
        private uint _endIndex;
        private uint _length;
        private int[] _elements;

        public ArrayVector()
        {
            this._startIndex = 0;
            this._endIndex   = 5;
            this._length     = this._endIndex - this._startIndex + 1;
            this._elements   = new int[this._length];
        }
        
        public ArrayVector(uint length)
        {
            this._startIndex = 0;
            this._endIndex   = length - 1;
            this._length     = length;            
            this._elements   = new int[this._length];    
        }
            
        public ArrayVector(uint startIndex, uint endIndex)
        {
            if(endIndex <= startIndex) 
            {
                Console.WriteLine("endIndex must be graeter than startIndex");
                return;
            }
            else 
            {
                this._startIndex = startIndex;
                this._endIndex   = endIndex;
                this._length     = endIndex - startIndex + 1;            
                this._elements   = new int[this._length];    
            }
        }    
        
        public ArrayVector(int[] array)
                :this ((uint)array.Length)
        {
            for (var i = 0; i < array.Length; i++)
            {                
                this._elements[i] = array[i];
            }                
        }
        
        public uint Length
        {
            get { return this._length; }
        }
        
        public uint StartIndex
        {
            get { return this._startIndex; }
        }
        
        public uint EndIndex
        {
            get { return this._endIndex; }
        }
        
        public int this[uint index]
        {
            get
            {
                try
                {
                    return this._elements[index - this._startIndex];
                }
                catch (IndexOutOfRangeException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Index of ArrayVector must be in range [{0}, {1}]", this._startIndex, this._endIndex);
                    return 0;
                }                
            }
            set
            {
                try
                {
                    this._elements[index - this._startIndex] = value;
                }
                catch (IndexOutOfRangeException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Index of ArrayVector must be in range [{0}, {1}]", this._startIndex, this._endIndex);                    
                }
            }
        }
        
		public static ArrayVector Add(ArrayVector fVector, ArrayVector sVector)
        {
            //If vactors has unequal length, throw ArgumentException
            if ( fVector.Length !=  sVector.Length )
            {
                throw new ArgumentException();
            }
            
            else 
            {            
				try
                {
                    ArrayVector vector = new ArrayVector(fVector.Length); 
                    for( uint i = 0; i < fVector.Length; i++)
                    {
                        vector[i] = fVector[i + fVector.StartIndex] + sVector[i + sVector.StartIndex];                    
                    }
                    return vector;
                }
                catch (IndexOutOfRangeException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("");    
                    return null;
                }    
            }
        }
        
        public static ArrayVector Diff(ArrayVector fVector, ArrayVector sVector)
        {
            //If vactors has unequal length, throw ArgumentException
            if ( fVector.Length !=  sVector.Length )
            {
                throw new ArgumentException();
            }            
            else 
            {            
                try
                {
                    ArrayVector vector = new ArrayVector(fVector.Length); 
                    for( uint i = 0; i < fVector.Length; i++)
                    {
                        vector[i] = fVector[i + fVector.StartIndex] - sVector[i + sVector.StartIndex];                    
                    }
                    return vector;
                }
                catch (IndexOutOfRangeException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("");    
                    return null;
                }    
            }
        }
                
        public static ArrayVector operator + (ArrayVector lhs, ArrayVector rhs)
        {
            return ArrayVector.Add(lhs, rhs);
        }
        
        public static ArrayVector operator - (ArrayVector lhs, ArrayVector rhs)
        {
            return ArrayVector.Diff(lhs, rhs);
        }
        
        public static ArrayVector operator * (int lhs, ArrayVector rhs)
        {            
            ArrayVector vector = new ArrayVector(rhs.Length); 
            for( uint i = 0; i < rhs.Length; i++)
            {
                vector[i] = lhs * rhs[i + rhs.StartIndex];                    
            }                        
            return vector;            
        }
        
        public static ArrayVector operator * (ArrayVector lhs, int rhs)
        {
            return rhs * lhs;
        }
        
        public static bool operator == (ArrayVector lhs, ArrayVector rhs)
        {
            return lhs.Equals(rhs);
        }
        
        public static bool operator != (ArrayVector lhs, ArrayVector rhs)
        {
            return !Equals(lhs, rhs);
        }
        
        public static bool operator > (ArrayVector lhs, ArrayVector rhs)
        {
            return (lhs.Length > rhs.Length);
        }
        
        public static bool operator < (ArrayVector lhs, ArrayVector rhs)
        {
            return (lhs.Length < rhs.Length);
        }
        
        public static bool operator >= (ArrayVector lhs, ArrayVector rhs)
        {
            return !(lhs < rhs);            
        }
        
        public static bool operator <= (ArrayVector lhs, ArrayVector rhs)
        {
            return !(lhs > rhs);
        }
        
        
        public override bool Equals(object obj)
        {
            // IP: ����� ����� ���������� ���������� �-��� "as":
			// ArrayVector tempArrayVector = obj as ArrayVector;
			// if (tempArrayVector != null)
			if(obj is ArrayVector && obj != null)
            {
                ArrayVector tempArrayVector = (ArrayVector) obj;
                if(tempArrayVector.Length  != this.Length ) 
                {
                    return false;
                }
                                
                // IP: ������� ������� �������� �� ��������� ����� ���������� � �-���, - � ��� ��� ���� �������� �-�� "return"
				bool equal = true;
                for( uint i = 0; i < tempArrayVector.Length; i++)
                {
                    if( this[i + this.StartIndex] != tempArrayVector[i + tempArrayVector.StartIndex] )
                    {
                        equal = false;
                    }
                }                
                return equal;    
            }
            else 
            {
                return false;
            }
        }        
        
        public override int GetHashCode()
        {
            int hashCode = 0;
            
            for( uint i = this._startIndex; i <= this._startIndex; i++)
            {
                hashCode = unchecked(hashCode * 13 + (int)this[i]);
            }
            
            return hashCode;
        }    
        
        
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            if(this._length > 0) 
            {
                result.Append("(");
                for(var i = _startIndex; i <= _endIndex; i++)
                {                
                    result.Append(String.Format("[{0}]={1} ", i, this._elements[i - _startIndex] ));    
                }
                result.Append(")");
                return result.ToString();
            }
            else 
            {
                return "object is undefined";    
            }
            
        }
        
    }    
    
}