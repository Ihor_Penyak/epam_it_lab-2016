using System;

namespace Task03
{
    class Program
    {
        
        static void Main()
        {
            Console.WriteLine("--------------------");
            
            var someArray = new int[5] {2, 5, 8, 10, 15};
                        
            //1. Vector Creation 
            
            Console.WriteLine("1. Vector Creation");
            
            //Constructor by default
            Console.WriteLine("Constructor by default");
            
            ArrayVector firstVector = new ArrayVector(); 
            
            Console.WriteLine(firstVector);
            Console.WriteLine();
            
            //Constructor with params ArrayVector(length)
            Console.WriteLine("Constructor with params ArrayVector(length)");
            
            ArrayVector secondVector = new ArrayVector(5); 
            
            Console.WriteLine(secondVector);
            Console.WriteLine();
                        
            //Constructor with params ArrayVector(startIndex, endIndex)
            Console.WriteLine("Constructor with params ArrayVector(startIndex, endIndex)");
            
            secondVector = new ArrayVector(2, 7);
            
            Console.WriteLine(secondVector);
            Console.WriteLine();
            
            //Set incorrect range of indexes ArrayVector(uint startIndex, uint endIndex)
            Console.WriteLine("Set incorrect range of indexes ArrayVector(uint startIndex, uint endIndex)");
            
            ArrayVector thirdVector = new ArrayVector(5, 1); 
            
            Console.WriteLine(thirdVector);
            Console.WriteLine();
            
            //Creating ArrayVector based on 1D array ArrayVector(int[] array)             
            Console.WriteLine("Creating ArrayVector based on 1D array ArrayVector(int[] array) ");
            
            ArrayVector fourthVector = new ArrayVector(someArray);
            
            Console.WriteLine(fourthVector);
            Console.WriteLine();
                                    
            
            //2. Setting/Getting values using indexer
            Console.WriteLine("2. Setting/Getting values using indexer");
            
            secondVector[2] = -3;
            secondVector[3] = 5;
            secondVector[4] = 10;
            secondVector[5] = -1;
            
            //Printing secondVector.ToString();
            Console.WriteLine("Printing secondVector.ToString():");
            Console.WriteLine(secondVector);
            
            Console.WriteLine("Or as regular array:");
            for (uint i = secondVector.StartIndex; i < secondVector.EndIndex; i++)
            {
                Console.Write("{0} ", secondVector[i]);
            }
            Console.WriteLine("--------------------");        
            Console.WriteLine();
            
            
            
            Console.WriteLine("Trying to access incorrect index");
            Console.Write("secondVector[1111] = {0}" , secondVector[1111]);
            Console.WriteLine();
                    
            Console.WriteLine("--------------------");                    
            //3. Operations over ArrayVector class
            Console.WriteLine("Operations over ArrayVector class");
            
            var firstArray  = new int[3] {2, 5, 8,};
            var secondArray = new int[3] {2, 0, 2 };
            var thirdArray  = new int[4] {2, 5, 8, 15};
            
            ArrayVector fifthVector   =  new ArrayVector(firstArray);
            ArrayVector sixthVector   =  new ArrayVector(secondArray);
            ArrayVector seventhVector =  new ArrayVector(thirdArray);
            ArrayVector sumVector, substractVector;
            
            
            //3.1 Summation 
            Console.WriteLine("3.1 Summation of vectors");
            Console.WriteLine();
            Console.WriteLine("Initial: fifthVector={0}; sixthVector={1}", fifthVector, sixthVector);
            Console.WriteLine();
            Console.WriteLine("1.1 Staic method ArrayVector.Add(fifthVector, sixthVector)");            
            sumVector = ArrayVector.Add(fifthVector, sixthVector);
            Console.WriteLine("Result (new object): sumVector={0}", sumVector);
            Console.WriteLine();
            
            Console.WriteLine("1.2 Overriden operator '+': fifthVector + sixthVector");
            sumVector = fifthVector + sixthVector;
            Console.WriteLine("Result (new object): sumVector={0}", sumVector);
            Console.WriteLine();
            
            Console.WriteLine("--------------------");
            
            //3.2 Subtraction 
            Console.WriteLine("3.2 Subtraction of vectors");
            
            Console.WriteLine("operator '-': sfifthVector + sixthVector");
            substractVector = fifthVector - sixthVector;             
            Console.WriteLine("Result (new object): substractVector={0}", substractVector);
            Console.WriteLine();
            
            
            Console.WriteLine("3.3 Multiplication by a scalar");
            Console.WriteLine("Overriden operator * : scalar * ArrayVector Or ArrayVector * scalar");
            
            int scalarValue = 3;
            Console.WriteLine("Initial: scalarValue={0}", scalarValue);
            Console.WriteLine("Initial: secondVector={0}", secondVector);            
            Console.WriteLine("Result:  secondVector * scalarValue = {0}", secondVector * scalarValue);
            Console.WriteLine("Result:  scalarValue * secondVector = {0}", scalarValue * secondVector);
            Console.WriteLine();
            Console.WriteLine("--------------------");
            
            
            
            // If two vector has unequal lenth ArgumentException threw 
            Console.WriteLine("If two vector has unequal lenth ArgumentException threw ");            
            try
            {                
                sumVector = ArrayVector.Add(seventhVector, fifthVector);
                Console.WriteLine(sumVector);
            }
            catch (ArgumentException e){
                Console.WriteLine(e);                
            }
            
            Console.WriteLine("--------------------");    
            Console.WriteLine();
                
            
            //4. Comparison
            Console.WriteLine("4. Comparison");
            
            Console.WriteLine("ArrayVector seventhVector {0} equal to fifthVector (overriden operator '==')", 
                                seventhVector == fifthVector  ? "is" : "is not" );
            
            Console.WriteLine("ArrayVector sixthVector {0} than seventhVector (overriden operator '>')", 
                                sixthVector > seventhVector  ? "is grater" : "is not grater" );
                                
            
            
            Console.ReadLine();
            
            return;
        }
        
        
    }
    
}