using System;

namespace Task01
{
    class Program
    {
        
        // just comment ...
		static void Main()
        {
            Console.WriteLine("--------------------");
            
            //Vectors initializing        
            Vector3D firstVector  = new Vector3D(); 
            Vector3D secondVector = new Vector3D(1,   -1, 2); 
            Vector3D thirdVector  = new Vector3D(5,   4,  3); 
            Vector3D fourthVector = new Vector3D(2.5, 2  ); 
            Vector3D fifthVector  = new Vector3D(10); 
                        
            
            //Changing vector's coordinates
            Console.WriteLine("Changing vector's coordinates:");
            Console.WriteLine("firstVector={0}", firstVector);
            Console.WriteLine("firstVector.X = -1; firstVector.Y = -5; firstVector.Z = 3;");
            firstVector.X = -1; 
            firstVector.Y = -5; 
            firstVector.Z = 3;
            Console.WriteLine("Result:");
            Console.WriteLine("firstVector={0}", firstVector);            
            Console.WriteLine("--------------------");
            
            //Operations on Vector3D class 
            //1. Summation of vectors secondVector and thirdVector
            Console.WriteLine("1. Summation of vectors");
            Console.WriteLine();
            Console.WriteLine("Initial: secondVector={0}; thirdVector={1}", secondVector, thirdVector);
            Console.WriteLine();
            Console.WriteLine("1.1 Staic method Vector3D.Add(secondVector, thirdVector)");            
            Vector3D sumOfVectors = Vector3D.Add(secondVector, thirdVector);
            Console.WriteLine("Result (new object): sumOfVectors={0}", sumOfVectors);
            Console.WriteLine();
            
            Console.WriteLine("1.2 Overriden operator '+': secondVector + thirdVector");
            sumOfVectors = secondVector + thirdVector;
            Console.WriteLine("Result (new object): sumOfVectors={0}", sumOfVectors);
            Console.WriteLine();
            
            Console.WriteLine("1.3 Object's metod Add(): secondVector.Add(thirdVector)");
            secondVector.Add(thirdVector);
            Console.WriteLine("Result: secondVector has been changed secondVector={0}", secondVector);
            
            Console.WriteLine("--------------------");
            
            //2. Subtraction of vectors secondVector and thirdVector
            Console.WriteLine("2. Subtraction of vectors");
            Console.WriteLine("Initial: secondVector={0}; thirdVector={1}", secondVector, thirdVector);
            Console.WriteLine();
            
            Console.WriteLine("2.1 Staic method Vector3D.Diff(secondVector, thirdVector)");            
            Vector3D diffOfVectors = Vector3D.Diff(secondVector, thirdVector);
            Console.WriteLine("Result (new object): diffOfVectors={0}", diffOfVectors);
            Console.WriteLine();
            
            Console.WriteLine("2.2 Overriden operator '-': secondVector - thirdVector");
            diffOfVectors = secondVector - thirdVector;
            Console.WriteLine("Result (new object): diffOfVectors={0}", diffOfVectors);
            Console.WriteLine();
            
            Console.WriteLine("2.3 Object's metod Diff(): secondVector.Diff(thirdVector)");
            secondVector.Diff(thirdVector);
            Console.WriteLine("Result: secondVector has been changed secondVector={0}", secondVector);
            
            Console.WriteLine("--------------------");
             
            //3. Multiplication of vectors thirdVector and fourthVector
            Console.WriteLine("3. Multiplication of vectors");
            
            Console.WriteLine("Initial: thirdVector={0}; fourthVector={1}", thirdVector, fourthVector);
            Console.WriteLine();
            
            Console.WriteLine("3.1 ScalarProduct by using static method Vector3D.ScalarProduct(thirdVector, fourthVector)");
            double scalarProductOfTwoVectors = Vector3D.ScalarProduct(thirdVector, fourthVector);
            Console.WriteLine("Result: scalarProductOfTwoVectors={0}", scalarProductOfTwoVectors);
            Console.WriteLine();
            
            Console.WriteLine("3.2 ScalarProduct by using Overriden operator * : thirdVector * fourthVector");
            scalarProductOfTwoVectors = thirdVector * fourthVector;
            Console.WriteLine("Result: scalarProductOfTwoVectors={0}", scalarProductOfTwoVectors);
            Console.WriteLine();
            
            Console.WriteLine("3.3 VectorProduct by using static method Vector3D.VectorProduct(thirdVector, fourthVector)");
            Vector3D vectorProduct = Vector3D.VectorProduct(thirdVector, fourthVector);
            Console.WriteLine("Result (new object): vectorProduct={0}", vectorProduct);
            Console.WriteLine();
            
            Console.WriteLine("3.4 TripleProduct by using static method Vector3D.TripleProduct(secondVector, thirdVector, fourthVector)");
            double tripleProduct = Vector3D.TripleProduct(secondVector, thirdVector, fourthVector);
            Console.WriteLine("Result: tripleProduct={0}", tripleProduct);
            Console.WriteLine();
            
            Console.WriteLine("3.5 Multiplication by a scalar");
            Console.WriteLine("Overriden operator * : scalar * Vector3D Or Vector3D * scalar");
            double scalarValue = 3.5;
            Console.WriteLine("Initial: scalarValue={0}", scalarValue);
            Console.WriteLine("Initial: secondVector={0}", secondVector);            
            Console.WriteLine("Result:  secondVector * scalarValue = {0}", secondVector * scalarValue);
            Console.WriteLine("Result:  scalarValue * secondVector = {0}", scalarValue * secondVector);
            
            Console.WriteLine("--------------------");
            
            //4. Vector Lenght
            Console.WriteLine("4. Vector Lenght");
            Console.WriteLine("Initial: firstVector={0}; secondVector={1}", firstVector, secondVector);
            Console.WriteLine("Use property 'Length': ");            
            Console.WriteLine("Vector firstVector lenght is {0:F4}", firstVector.Length);
            Console.WriteLine("Vector secondVector lenght is {0:F4}", secondVector.Length);
            
            Console.WriteLine("--------------------");
            
            //5. Angles between two vectors
            Console.WriteLine("5. Angles between two vectors");
            Console.WriteLine("We can use static or object's methods");
            
            Console.WriteLine("Angle between secondVector and thirdVector: Vector3D.AngleBetween(secondVector, thirdVector) = {0:F4}", 
                                Vector3D.AngleBetween(secondVector, thirdVector) );
            Console.WriteLine("Angle between secondVector and thirdVector: secondVector.AngleBetween(thirdVector) = {0:F4}", 
                                secondVector.AngleBetween(thirdVector) );
            
            Console.WriteLine("--------------------");
            
            //6. Vector comparison
            Console.WriteLine("6. Vector comparison");
            
            Vector3D sixthVector   = new Vector3D(3, 4, 5);
            Vector3D seventhVector = new Vector3D(3, 4, 5);
            Vector3D eighthVector  = new Vector3D(0, 2, 4);
            
            Console.WriteLine("Initial:" );
            Console.WriteLine("sixthVector={0}", sixthVector);
            Console.WriteLine("seventhVector={0}", seventhVector);
            Console.WriteLine("eighthVector={0}", eighthVector);
                        
            Console.WriteLine("Vector sixthVector {0} equal to seventhVector (overriden operator '==')", 
                                sixthVector == seventhVector  ? "is" : "is not" );
            Console.WriteLine("Vector sixthVector {0} equal to seventhVector (method Equals)", 
                                sixthVector.Equals(seventhVector)  ? "is" : "is not" );
            Console.WriteLine("Vector sixthVector {0} equal to seventhVector (static method Equals)", 
                                Vector3D.Equals(sixthVector, seventhVector)  ? "is" : "is not" );
            Console.WriteLine();
            
            Console.WriteLine("Vector sixthVector {0} equal to eighthVector (overriden operator '!=')", 
                                sixthVector != eighthVector  ? "is not" : "is" );
            Console.WriteLine();    
            
            Console.WriteLine("Vector sixthVector {0} than eighthVector (overriden operator '>')", 
                                sixthVector > eighthVector  ? "is grater" : "is not grater" );
            Console.WriteLine("Vector sixthVector {0} than eighthVector (method GreaterThan)", 
                                sixthVector.GreaterThan(eighthVector) ? "is greater " : "is not greater" );
            Console.WriteLine();
            
            Console.WriteLine("Vector sixthVector {0} than eighthVector (overriden operator '<')", 
                                sixthVector < eighthVector  ? "is less" : "is not less" );
            Console.WriteLine("Vector sixthVector {0} than eighthVector (method LessThan)", 
                                sixthVector.LessThan(eighthVector) ? "is less " : "is not less" );
            Console.WriteLine();
            
            Console.WriteLine("Vector sixthVector {0} than eighthVector (overriden operator '>=')", 
                                sixthVector >= eighthVector  ? "is grater or equal" : "is not grater or equal" );
            Console.WriteLine("Vector sixthVector {0} than eighthVector (method GreaterThanOrEqualTo)", 
                                sixthVector.GreaterThanOrEqualTo(eighthVector) ? "is grater or equal" : "is not grater or equal" );
            Console.WriteLine();
            
            Console.ReadLine();
            
            return;
        }
        
        
    }
    
}