using System;

namespace Task01
{
    // IP: ���� ����������� ���� � ���������� ����� �� "public"
	class Vector3D
    {
                                
        public Vector3D(double xValue = 0, double yValue = 0, double zValue = 0)
        {
            this.X = xValue;
            this.Y = yValue;
            this.Z = zValue;
        }
        
        public Vector3D(Vector3D rhs)
        {
            this.X = rhs.X;
            this.Y = rhs.Z;
            this.Z = rhs.Z;            
        }        
        
		public double X{ get; set; }
        public double Y{ get; set; }
        public double Z{ get; set; }
		public double Length
        {
            get 
            {
                return Math.Sqrt(this.X * this.X + this.Y * this.Y + this.Z * this.Z);
            }
        }
        
        public static Vector3D Add(Vector3D fVecor, Vector3D sVecor)
        {
            return new Vector3D(fVecor.X + sVecor.X,
                                fVecor.Y + sVecor.Y,
                                fVecor.Z + sVecor.Z                                
                                );
        }
        
        public void Add(Vector3D fVecor)
        {
            this.X += fVecor.X;
            this.Y += fVecor.Y;
            this.Z += fVecor.Z;            
        }
        
        public static Vector3D Diff(Vector3D fVecor, Vector3D sVecor)
        {
            return new Vector3D(fVecor.X - sVecor.X,
                                fVecor.Y - sVecor.Y,
                                fVecor.Z - sVecor.Z                                
                                );
        }
        
        public void Diff(Vector3D fVecor)
        {
            this.X -= fVecor.X;
            this.Y -= fVecor.Y;
            this.Z -= fVecor.Z;            
        }
        
        public static double ScalarProduct(Vector3D fVecor, Vector3D sVecor)
        {
            return (fVecor.X * sVecor.X + fVecor.Y * sVecor.Y + fVecor.Z * sVecor.Z);
        }
        
        public static Vector3D VectorProduct(Vector3D fVecor, Vector3D sVecor)
        {
            return new Vector3D(fVecor.Y * sVecor.Z - fVecor.Z * sVecor.Y, 
                                fVecor.Z * sVecor.X - fVecor.X * sVecor.Z, 
                                fVecor.X * sVecor.Y - fVecor.Y * sVecor.X 
                                );            
        }
        
        public static double TripleProduct(Vector3D fVecor, Vector3D sVecor, Vector3D thVecor)
        {
            return fVecor * Vector3D.VectorProduct(sVecor, thVecor);
        }
            
        public static double Cos(Vector3D fVecor, Vector3D sVecor)
        {            
            return (fVecor * sVecor) / 
                   (fVecor.Length * sVecor.Length);
        }
        
        public double Cos(Vector3D fVecor)
        {            
            return (this * fVecor) / 
                   (this.Length * fVecor.Length);
        }

        public static double AngleBetween(Vector3D fVecor, Vector3D sVecor)
        {
            double arcCos = Math.Acos(Vector3D.Cos(fVecor, sVecor));
            if(!double.IsNaN(arcCos))
            {
                return arcCos * (180.0 / Math.PI);
            }    
            else 
            {
                Console.WriteLine("Vectors are incorrect. Could not calculate angle");
				// IP: ��������� return ������� ����������
            }
            return arcCos * (180.0 / Math.PI);            
        }

        public double AngleBetween(Vector3D fVector)
        {
            return Math.Acos(this.Cos(fVector)) * (180.0 / Math.PI);
        }    
        
        public bool LessThan(Vector3D fVector)
        {
            return (this.Length < fVector.Length);            
        }
        
        public bool LessThanOrEqualTo(Vector3D fVector)
        {
            return !(this.Length > fVector.Length);                
        }
        
        public bool GreaterThan(Vector3D fVector)
        {
            return (this.Length > fVector.Length);                
        }
        
        public bool GreaterThanOrEqualTo(Vector3D fVector)
        {
            // IP: ��������� ���������� �-��� ���� ���� ����� ������� ����. �-���:
			// return !LessThan(fVector);
			return !(this.Length < fVector.Length);                
        }
        
        public override bool Equals(object obj)
        {
            // IP: ���� ��������� ������� ����. �����:
			/*
			            Vector3D tempVector3D = obj as Vector3D;
            
			            return  (tempVector3D != null
			                && (this.X == tempVector3D.X) &&
			                       (this.Y == tempVector3D.Y) &&
			                       (this.Z == tempVector3D.Z));
			*/
			if(obj is Vector3D && obj != null)
            {
                Vector3D tempVector3D = (Vector3D) obj;
                
                return (this.X == tempVector3D.X) &&
                       (this.Y == tempVector3D.Y) &&
                       (this.Z == tempVector3D.Z);                
            }
            else 
            {
                return false;
            }
        }        
        
        public override int GetHashCode()
        {
            return (int)this.X.GetHashCode() * 10 + (int)this.Y.GetHashCode() * 11 + (int)this.Z.GetHashCode() * 12;
        }        
        
        public static bool operator == (Vector3D lhs, Vector3D rhs)
        {
            return lhs.Equals(rhs);
        }
        
        public static bool operator != (Vector3D lhs, Vector3D rhs)
        {
            return !Equals(lhs, rhs);
        }
        
        // IP: �-��� ��������� ������� ���������� ���. �����
        public static bool operator > (Vector3D lhs, Vector3D rhs)
        {
            return (lhs.Length > rhs.Length);
        }
        
        public static bool operator < (Vector3D lhs, Vector3D rhs)
        {
            return (lhs.Length < rhs.Length);
        }
        
        public static bool operator >= (Vector3D lhs, Vector3D rhs)
        {
            return !(lhs < rhs);            
        }
        
        public static bool operator <= (Vector3D lhs, Vector3D rhs)
        {
            return !(lhs > rhs);
        }
        
                
        public static Vector3D operator + (Vector3D lhs, Vector3D rhs)
        {
            // IP: ������ ������� ������� ���:
			// return new Vector3D(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z);
			
			Vector3D result = new Vector3D(lhs);
            result.X += rhs.X;
            result.Y += rhs.Y;
            result.Z += rhs.Z;
            
			// IP: �������� �-�� ���������� � �-��� ������ ������� �� ����� ���� ���������� ������ ������ (�� ����� �-�� ���� ���� � �-��� �� ���� ...)
			return result;            
        }
        
        public static Vector3D operator - (Vector3D lhs, Vector3D rhs)
        {
            Vector3D result = new Vector3D(lhs);
            result.X -= rhs.X;
            result.Y -= rhs.Y;
            result.Z -= rhs.Z;
            return result;            
        }
        
        public static Vector3D operator - (Vector3D lhs)
        {
            return -1 * lhs;            
        }
        
        public static Vector3D operator * (double lhs, Vector3D rhs)
        {            
            return new Vector3D(lhs * rhs.X, lhs * rhs.Y, lhs * rhs.Z);            
        }
        
        public static Vector3D operator * (Vector3D lhs, double rhs)
        {
            return rhs * lhs;
        }
        
        public static double operator * (Vector3D lhs, Vector3D rhs) //Scalar product
        {
            return (lhs.X * rhs.X + lhs.Y * rhs.Y + lhs.Z * rhs.Z);
        }
            
        
        public override string ToString()
        {
            return string.Format("({0}, {1}, {2})", X, Y, Z);
        }
            
        
    }
    
    
}