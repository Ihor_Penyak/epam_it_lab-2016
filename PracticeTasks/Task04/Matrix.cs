using System;
using System.Text;

namespace Task04
{
    class Matrix
    {
        private double [][] _elements;
        private uint _rows;
        private uint _cols;
        
        public Matrix()
        {        
            this._cols = 2;
            this._rows = 2;
            this._elements = new double[this._rows][];
            for(uint i = 0; i < this._rows; i++) 
            {
                this._elements[i] = new double[this._cols];
            }
            
        }
                
        //Fill matrix by zeroes
        public Matrix(uint rows, uint cols)
        {
            if(cols == 0 || rows == 0) 
            {
                Console.WriteLine("Incorrect Dimentions");
                return;
            }
            this._cols = cols;
            this._rows = rows;
            this._elements = new double[rows][];
            for(uint i = 0; i < rows; i++) 
            {
                this._elements[i] = new double[cols];
            }    
        }
        
        //Square matrix
        public Matrix(uint rows)
                : this(rows, rows)
        {            
        }
                
        //Fill matrix elements of some scalar value         
        public Matrix(uint rows, uint cols, double scalar)                
        {
            this._cols = cols;
            this._rows = rows;
            this._elements = new double[rows][];
            for(uint i = 0; i < rows; i++) 
            {
                this._elements[i] = new double[cols];
            }
            for(uint i = 0; i < rows; i++) 
            {
                for(uint j = 0; j < cols; j++) 
                {
                    this._elements[i][j] = scalar;
                }
            }        
        }
        
        
        public Matrix(double[][] array)
        {
            this._rows = (uint)array.Length;
            this._cols = (uint)array[0].Length;
            
            
            for(uint i = 0; i < this._rows; i++) 
            {
                if(array[i].Length != this._cols)
                {
                    throw new ArgumentException("Rows must have equal length");                    
                }
            }    
            
            this._elements = array;
        }
        
        public Matrix(double[,] array)
        {
            this._rows = (uint)array.GetLength(0);
            this._cols = (uint)array.GetLength(1);
                        
            this._elements = new double[this._rows][];
            for(uint i = 0; i < this._rows; i++) 
            {
                this._elements[i] = new double[this._cols];
                for(uint j = 0; j < this._cols; j++) 
                {
                    this._elements[i][j] = array[i, j];    
                }                
            }
        }
        
        //Copy constructor
        public Matrix(Matrix m)
        {
            this._rows     = m.Rows;
            this._cols     = m.Cols;
            this._elements = m.ElementsCopy;            
        }
        
        public double this[uint i, uint j]
        {            
            get
            {
                // IP: �������� ������ ���� � ���������� 
				
				/*if (i < 0 || i >= this._cols || j < 0 || j >= this._rows)
                {
                    throw new IndexOutOfRangeException();
                }
                return this._elements[i][j];*/
                try 
                {
                    return this._elements[i][j];
                }
                catch(IndexOutOfRangeException e) 
                {
                    Console.WriteLine(e.Message);
                    return 0;
                }
                
            }            
            set
            {
                /*if (i < 0 || i >= this._cols || j < 0 || j >= this._rows)
                {
                    throw new IndexOutOfRangeException();
                } */
                try 
                {
                    this._elements[i][j] = value;
                }
                catch(IndexOutOfRangeException e) 
                {
                    Console.WriteLine(e.Message);                    
                }                
                
            }
        }
        
        public uint Rows
        {
            get
            {
                return this._rows;
            }            
        }
        
        public uint Cols
        {
            get
            {
                return this._cols;
            }
            
        }
        
        public double[][] Elements
        {
            get
            {
                return this._elements;
            }
        }
        
        public double[][] ElementsCopy
        {
            get
            {
                var copy = new double[this._rows][];
                for (uint i = 0; i < this._rows; i++)
                {
                    copy[i] = new double[this._cols];
                }
                for (uint i = 0; i < this._rows; i++)
                {
                    for (uint j = 0; j < this._cols; j++)
                    {
                        copy[i][j] = this._elements[i][j];
                    }
                }
                return copy;
            }
        }
        
        public double Norm
        {
            
            get
            {
                double norm = 0;
                
                for (uint j = 0; j < this._cols; j++)
                {
                    double sumOfRow = 0;
                    
                    for (uint i = 0; i < this._rows; i++)
                    {
                        sumOfRow += Math.Abs( this[i, j] );
                    }
                    norm = Math.Max(norm, sumOfRow);
                }
                
                return norm;
            }
        }
        
        public static Matrix RandomMarix(uint rows, uint cols, int measurement = 5)
        {
            Random random = new Random();
            Matrix matrix = new Matrix(rows, cols);    
            for(uint i = 0; i < rows; i++) 
            {                
                for(uint j = 0; j < cols; j++) 
                {
                    matrix[i, j] = random.Next(-measurement, measurement);    
                }                
            }
            return matrix;
            
        }
        
        public Matrix Transponse()
        {
            Matrix matrix = new Matrix(this._cols, this._rows);
            double[][] elements = matrix.Elements;
            for (uint i = 0; i < this._rows; i++)
            {
                for (uint j = 0; j < this._cols; j++)
                {
                    elements[j][i] = this._elements[i][j];
                }
            }
            return matrix;            
        }
        
        public static Matrix Add(Matrix a, Matrix b)
        {
            try {
                Matrix.CompareDimensions(a, b);
                
                Matrix matrix = new Matrix(a.Rows, a.Cols);
                double[][] elements = matrix.Elements;
                
                for (uint i = 0; i < a.Rows; i++)
                {
                    for (uint j = 0; j < a.Cols; j++)
                    {
                        elements[i][j] = a[i, j] + b[i, j];
                    }
                }
                return matrix;
            }
            catch( ArgumentException e )
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        
        public Matrix Add(Matrix a)
        {
            try {
                this.CompareDimensions(a);
                
                for (uint i = 0; i < this._rows; i++)
                {
                    for (uint j = 0; j < this._cols; j++)
                    {
                        this._elements[i][j] += a[i, j];
                    }
                }
                return this;
            }
            catch( ArgumentException e )
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        
        public static Matrix Diff(Matrix a, Matrix b)
        {
            try {
                Matrix.CompareDimensions(a, b);
                
                Matrix matrix = new Matrix(a.Rows, a.Cols);
                double[][] elements = matrix.Elements;
                
                for (uint i = 0; i < a.Rows; i++)
                {
                    for (uint j = 0; j < a.Cols; j++)
                    {
                        elements[i][j] = a[i, j] - b[i, j];
                    }
                }
                return matrix;
            }
            catch( ArgumentException e )
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        
        public Matrix Diff(Matrix a)
        {
            try {
                this.CompareDimensions(a);
                
                for (uint i = 0; i < this._rows; i++)
                {
                    for (uint j = 0; j < this._cols; j++)
                    {
                        this._elements[i][j] -= a[i, j];
                    }
                }
                return this;
            }
            catch( ArgumentException e )
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        
        
        public static Matrix Product(Matrix a, Matrix b)
        {
            try {
                
                CompareColumnsAndRowsDimensions(a, b);
                
                Matrix matrix = new Matrix(a.Rows, b.Cols);                
                double[][] elements = matrix.Elements;
                
                var bColumns = new double[a.Cols];
                
                for (uint j = 0; j < b.Cols; j++)
                {
                    for (uint k = 0; k < a.Cols; k++)
                    {
                        bColumns[k] = b[k, j];
                    }
                    for (uint i = 0; i < a.Rows; i++)
                    {
                        double[] aRows = a.Elements[i];
                        double sum = 0;
                        
                        for (uint k = 0; k < a.Cols; k++)
                        {
                            sum += aRows[k] * bColumns[k];
                        }
                        elements[i][j] = sum;
                    }
                }
                return matrix;
            
            }
            catch( ArgumentException e )
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        
        public static Matrix Product(double scalar, Matrix a)
        {
            Matrix matrix = new Matrix(a.Rows, a.Cols);                
            double[][] elements = matrix.Elements;
            
            for (uint i = 0; i < a.Rows; i++)
            {
                for (uint j = 0; j < a.Cols; j++)
                {                    
                    elements[i][j] = a[i, j] * scalar;
                }
            }
            return matrix;
        }
        
        public static Matrix Product(Matrix a, double scalar)
        {
            return Matrix.Product(scalar, a);
        }
        
        public Matrix Product(double scalar)
        {
            
            for (uint i = 0; i < this._rows; i++)
            {
                for (uint j = 0; j < this._cols; j++)
                {
                    this._elements[i][j] *= scalar;
                }
            }
            return this;            
        }
        
        public bool LessThan(Matrix a)
        {
            return (this.Norm < a.Norm);            
        }
        
        public bool LessThanOrEqualTo(Matrix a)
        {
            return !(this.Norm > a.Norm);                
        }
        
        public bool GreaterThan(Matrix a)
        {
            return (this.Norm > a.Norm);                
        }
        
        public bool GreaterThanOrEqualTo(Matrix a)
        {
            return !(this.Norm < a.Norm);                
        }
        
        public static Matrix operator + (Matrix lhs, Matrix rhs)
        {
            return Matrix.Add(lhs, rhs);
        }
        
        public static Matrix operator - (Matrix lhs, Matrix rhs)
        {
            return Matrix.Diff(lhs, rhs);
        }
        
        public static Matrix operator * (Matrix lhs, Matrix rhs)
        {
            return Matrix.Product(lhs, rhs);
        }
        
        public static Matrix operator * (double lhs, Matrix rhs)
        {
            return Matrix.Product(lhs, rhs);
        }
        
        public static Matrix operator * (Matrix lhs, double rhs)
        {
            return lhs * rhs;
        }
        
        public static bool operator == (Matrix lhs, Matrix rhs)
        {
            return lhs.Equals(rhs);
        }
        
        public static bool operator != (Matrix lhs, Matrix rhs)
        {
            return !lhs.Equals(rhs);
        }
        
        public static bool operator > (Matrix lhs, Matrix rhs)
        {
            return (lhs.Norm > rhs.Norm);
        }
        
        public static bool operator < (Matrix lhs, Matrix rhs)
        {
            return (lhs.Norm < rhs.Norm);
        }
        
        public static bool operator >= (Matrix lhs, Matrix rhs)
        {
            return !(lhs < rhs);            
        }
        
        public static bool operator <= (Matrix lhs, Matrix rhs)
        {
            return !(lhs > rhs);
        }
        
        public bool Equals(Matrix other)
        {
            if (ReferenceEquals(null, other)) 
            {
                return false;
            }
            
            if (ReferenceEquals(this, other))
            {
                return true;
            }    
            
            if (this._cols != other.Cols || this._rows != other.Rows)
            {
                return false;
            }                
            
            bool equal = true;
            
            for (uint i = 0; i < this._rows; i++)
            {
                for (uint j = 0; j < this._cols; j++)
                {
                    if (this[i, j] != other[i, j])
                    {
                        equal = false;
                        break;
                    }
                        
                }
            }
            
            return equal;
        }

        public override bool Equals(object obj)
        {
            // IP:  � ���� � �� �-���� �-�� "switch"
			if (ReferenceEquals(null, obj)) 
            {
                return false;
            } 
            
            if (ReferenceEquals(this, obj))
            {
                return true;
            }    
            
            if (obj.GetType() != typeof (Matrix)) 
            {
                return false;
            }
                            
            return Equals((Matrix) obj);
        }

        public override int GetHashCode()
        {
            int hCode = 0;
            
            for (uint i = 0; i < this._rows; i++)
            {
                for (uint j = 0; j < this._cols; j++)
                {
                    hCode = unchecked(hCode * 113 + (int)this[i, j]);
                }
            }            
            
            return hCode;            
        }
        
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            
            for(uint i = 0; i < this._rows; i++)
            {
                for(uint j = 0; j < this._cols; j++)
                {
                    result.Append(String.Format("{0,3}", this._elements[i][j]));
                }
                result.AppendLine();
            }
            
            return result.ToString();
        }
        
        private static void CompareDimensions(Matrix a, Matrix b)
        {
            if (a.Rows != b.Rows || a.Cols != b.Cols)
            {
                throw new ArgumentException("Matrices has different dimensions");
            }
        }
        
        private void CompareDimensions(Matrix m)
        {
            if (this._rows != m.Rows || this._cols != m.Cols)
            {
                throw new ArgumentException("Matrices has different dimensions");
            }
        }
        
        private static void CompareColumnsAndRowsDimensions(Matrix a, Matrix b)
        {
            if (a.Rows != b.Cols || a.Cols != b.Rows)
            {
                throw new ArgumentException("Number of Rows and columns must be the same");
            }
        }        
    }   
    
}