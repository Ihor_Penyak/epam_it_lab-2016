using System;

namespace Task04
{
    class Program
    {
        
        static void Main()
        {
            double[,] firstTwoDimensionalArray = 
            {
                {1, 2, 5, 7},
                {2, 9, 1, 0}
            };
            
            double[,] secondTwoDimensionalArray = 
            {
                {5, 8, -10},
                {2, 0, 15 },
                {3, 8, 0  }
            };
            
            double[,] thirdTwoDimensionalArray = 
            {
                {5, 8},
                {2, 0},
                {3, 8},
                {2, 4}
            };
            
            double[][] firstJaggedArray = new double[][] 
                {
                    new double[] {1, 3, 5, 7},
                    new double[] {0, 2, 4, 6}                    
                };
            
            double[][] secondJaggedArray = 
                {
                    new double[] {1, 3, 5},
                    new double[] {0, 2, 4},
                    new double[] {5, 2, 0}
                };
            
            //1. Creating Matrices by calling different constructors
            //Constructor by default fills 2x2 with zeroes
            Matrix firstMatrix = new Matrix();
            
            Console.WriteLine("Constructor by default");
            Console.WriteLine(firstMatrix);
            
            //Constructor with dimention values 
            Matrix secondMatrix = new Matrix(3, 2);
            
            //And we can set some values with indexer
            secondMatrix[0, 0] = 11;
            secondMatrix[0, 2] = 5;            
            secondMatrix[1, 0] = -1;
            
            Console.WriteLine("Special constructor");
            Console.WriteLine(secondMatrix);
            
            //Create matrices based on two dimensional or jagged arrays
            Matrix thirdMatrix  = new Matrix(firstTwoDimensionalArray);
            Matrix fourthMatrix = new Matrix(firstJaggedArray);
            
            Console.WriteLine("Matrice based on 2d array:");
            Console.WriteLine(thirdMatrix);
            
            Console.WriteLine("Matrice based on jagged array:");
            Console.WriteLine(fourthMatrix);
            
            //Create matrice using copy constructor
            Matrix fifthMatrix = new Matrix(fourthMatrix);
            
            fifthMatrix[0, 0] = 55; //testing if elements have being copied into another array of elements 
            
            Console.WriteLine("Matrice using copy constructor:");
            Console.WriteLine(fourthMatrix);
            Console.WriteLine(fifthMatrix);
            
            //Static method that return randomly filled matrice in range -5;5 
            Matrix sixthMatrix = Matrix.RandomMarix(4, 5, 5);
            
            Console.WriteLine("Randomly filled matrice");
            Console.WriteLine(sixthMatrix);
            
            
            //2. Operation with matrices
            
            //2.1 Summation of matrices thirdMatrix and secondMatrix
            // Staic method, overriden operator '+', object's method that updates values  
            
            Console.WriteLine("Summation of matrices thirdMatrix and fourthMatrix");
            
            Console.WriteLine( Matrix.Add(thirdMatrix, fourthMatrix) );
            
            Console.WriteLine( thirdMatrix + fourthMatrix );
            
            Console.WriteLine( thirdMatrix.Add(fourthMatrix) );
            
            //If matrices have different dimentions, ArgumentException is threw, that is intercepted:
            Console.WriteLine("If matrices have different dimentions, ArgumentException is throwed, that is intercepted:");
            Console.WriteLine( secondMatrix + fourthMatrix  );
            
            //2.2 Subtraction of matrices thirdMatrix and secondMatrix
            //Similar approach            
            
            Console.WriteLine("Subtraction of matrices thirdMatrix and fourthMatrix");
            
            Console.WriteLine( Matrix.Diff(thirdMatrix, fourthMatrix) );
            
            Console.WriteLine( thirdMatrix - fourthMatrix );
            
            Console.WriteLine( thirdMatrix.Diff(fourthMatrix) );
            
            //2.3 Multiplication of two matrices
            Console.WriteLine( "Multiplication of two matrices by using static function" );    
            
            Matrix productOfMatrices = Matrix.Product(new Matrix(firstTwoDimensionalArray), 
                                                      new Matrix(thirdTwoDimensionalArray) );
            
            Console.WriteLine( productOfMatrices );
            
            Console.WriteLine("Or by using operator '*': ");    
            
            Console.WriteLine( new Matrix(firstTwoDimensionalArray) * new Matrix(thirdTwoDimensionalArray) );                                              
            
            //If dimentions are not appropriate, ArgumentException is threw
            Console.WriteLine("If dimentions are not appropriate, ArgumentException is threw:");
            Matrix.Product(thirdMatrix, firstMatrix);
            
            
            //2.4 Multiplication matrices by a scalar value
            Console.WriteLine("Multiplication of matrices by a scalar value");
            
            //a) Using objects method
            Console.WriteLine("Using objects method");
            Console.WriteLine("secondMatrix.Product(3)");
                        
            Console.WriteLine( secondMatrix.Product(3) );

            //b) Using static method - returns new Matrix
            Console.WriteLine("Using static method - returns new Matrix");
            Console.WriteLine("Matrix.Product(5, secondMatrix) OR Matrix.Product(secondMatrix, 5)");
            
            Console.WriteLine( Matrix.Product(5, secondMatrix) );
            
            //c) Using operator '*'            
            Console.WriteLine("Using operator '*' - returns new Matrix");
            Console.WriteLine("3 * secondMatrix) OR secondMatrix * 3");
            
            Console.WriteLine( Matrix.Product(3, secondMatrix) );
            
            //2.5 Matrice Transponse
            Console.WriteLine("Matrice Transponse");
            
            Matrix randomMatrix = Matrix.RandomMarix(2, 4, 5);
            
            Console.WriteLine("Original matrice:");
            Console.WriteLine(randomMatrix);
            
            Console.WriteLine("Transponded matrice:");
            Console.WriteLine(randomMatrix.Transponse());
            
            
            //3. Comparison of matrices
            Console.WriteLine("3. Comparison of matrices");
            
            double[,] fCompareArray = {
                                        {5, 8},
                                        {2, 0}                                        
                                      };
                                      
            double[,] sCompareArray = {
                                        {5, 8},
                                        {2, 0}                                        
                                      };
                                      
            double[,] thCompareArray = {
                                        {5, 8},
                                        {2, 0},
                                        {2, 1}                                        
                                      };                                
            
            Matrix fCompareMatrix  = new Matrix(fCompareArray);            
            Matrix sCompareMatrix  = new Matrix(sCompareArray);
            Matrix thCompareMatrix = new Matrix(thCompareArray); 
            
            Console.WriteLine("Compare by elements");
            
            Console.WriteLine("Matrice fCompareMatrix {0} equal to sCompareMatrix (operator '==')", fCompareMatrix == sCompareMatrix  ? "is" : "is not" );
            Console.WriteLine("Matrice fCompareMatrix {0} equal to sCompareMatrix (method Equals)", fCompareMatrix.Equals(sCompareMatrix)  ? "is" : "is not" );
                        
            Console.WriteLine("Matrice fCompareMatrix {0} equal to sCompareMatrix (operator '!=')", fCompareMatrix != sCompareMatrix  ? "is not" : "is" );
            Console.WriteLine("Matrice fCompareMatrix {0} equal to thCompareMatrix (operator '!=')", fCompareMatrix != thCompareMatrix  ? "is not" : "is" );
            
            Console.WriteLine("Compare by Norm");
            
            Console.WriteLine("Matrice fCompareMatrix {0} than sCompareMatrix (operator '>')", 
                                fCompareMatrix > sCompareMatrix  ? "is grater" : "is not grater" );
            Console.WriteLine("Matrice fCompareMatrix {0} than sCompareMatrix (method GreaterThan)", 
                                fCompareMatrix.GreaterThan(sCompareMatrix) ? "is greater " : "is not greater" );
            Console.WriteLine();
            
            Console.WriteLine("Matrice fCompareMatrix {0} than sCompareMatrix (operator '<')", fCompareMatrix < sCompareMatrix  ? "is less" : "is not less" );
            Console.WriteLine("Matrice fCompareMatrix {0} than sCompareMatrix (method LessThan)", fCompareMatrix.LessThan(sCompareMatrix) ? "is less " : "is not less" );
            Console.WriteLine();
            
            Console.WriteLine("Matrice fCompareMatrix {0} than thCompareMatrix (operator '>=')", fCompareMatrix >= thCompareMatrix  ? "is grater or equal" : "is not grater or equal" );
            Console.WriteLine("Matrice fCompareMatrix {0} than thCompareMatrix (method GreaterThanOrEqualTo)", fCompareMatrix.GreaterThanOrEqualTo(thCompareMatrix) ? "is grater or equal" : "is not grater or equal" );
            Console.WriteLine();
            
            Console.ReadLine();
            
            return;
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
}